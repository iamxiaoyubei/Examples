package com.ly.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Shower on 2017/4/2 0002.
 */
@ComponentScan
@Configuration
@EnableWebMvc
public class WebConfig {
}
