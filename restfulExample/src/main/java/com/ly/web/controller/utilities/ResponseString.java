package com.ly.web.controller.utilities;

/**
 * Created by Shower on 2017/4/2 0002.
 */
public class ResponseString {
    private String response;

    public ResponseString(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }
}
