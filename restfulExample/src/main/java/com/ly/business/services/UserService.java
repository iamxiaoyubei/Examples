package com.ly.business.services;

import com.ly.business.entities.User;
import com.ly.business.entities.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Shower on 2017/4/2 0002.
 */
@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User find(long id) {
        return userRepository.find(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public boolean add(User u) {
        return userRepository.add(u);
    }

    public User update(User u) {
        return userRepository.update(u);
    }

    public User delete(long id) {
        return userRepository.delete(id);
    }
}
