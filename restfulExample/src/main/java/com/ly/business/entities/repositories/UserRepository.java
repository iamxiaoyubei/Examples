package com.ly.business.entities.repositories;

import com.ly.business.entities.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Shower on 2017/4/2 0002.
 */
@Repository
public class UserRepository {
    private final Map<Long, User> users = new HashMap<Long, User>();

    public UserRepository() {}

    public User find(long id) {
        return users.get(id);
    }

    public List<User> findAll() {
        return new ArrayList<User>(users.values());
    }

    public boolean add(User u) {
        if (users.get(u.getId()) != null) return false;
        users.put(u.getId(), u);
        return true;
    }

    public User delete(Long id) {
        return users.remove(id);
    }

    public User update(User u) {
        return users.replace(u.getId(), u);
    }
}
