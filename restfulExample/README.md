# 一个restful的Web服务

实现了对User的增加，删除，修改，查找

运行： `mvn tomcat7:run`

----------
## 增加User

### URL

http://localhost:8080/restful/user

### 请求方式

PUT

### 请求参数

  |类型
--|--
id|long
firstName|string
lastName|string

----------
## 获取指定id的User

### URL



http://localhost:8080/restful/user/{id}

### 请求方式

GET

### 请求参数

无


----------
## 获取所有User

### URL

http://localhost:8080/restful/user

### 请求方式

GET

### 请求参数

无

----------
## 修改User

### URL

http://localhost:8080/restful/user/{id}

### 请求方式

POST

### 请求参数

  |类型
--|--
id|long
firstName|string
lastName|string

----------
## 删除User

### URL

http://localhost:8080/restful/user/{id}

### 请求方式

DELETE

### 请求参数

无